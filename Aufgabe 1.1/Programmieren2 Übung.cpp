// Programmieren2 Übung.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Wassertank.h"


int main()
{
	Wassertank* Tank = new Wassertank();

	int Tankgroesse;
	printf("Wie viele Liter fasst der Tank\n");
	scanf_s("%d", &Tankgroesse); fflush(stdin); getchar();
	Tank->setGroesse(Tankgroesse);


	while (true){
		char optionInput;
		printf("Was moechen Sie tun? [einfuellen=e, ausleeren=a, beenden=x]\n");
		scanf_s("%c", &optionInput); fflush(stdin); getchar();

		switch (optionInput)
		{
		case 'e':
			int menge;
			printf("Wieviele Lieter moechen Sie einfuellen?\n");
			scanf_s("%d", &menge); fflush(stdin); getchar();
			Tank->fuellen(menge);
			break;
		case 'a':
			Tank->ausleeren();
			break;
		case 'x':
			return 0;
			break;
		default:
			break;
		}

		printf("aktueller Fuellstand: %d\n", Tank->getFuellstand());
	}
    return 0;
}

