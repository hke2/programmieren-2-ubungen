#include "stdafx.h"
#include "Wassertank.h"


Wassertank::Wassertank()
{
	groesse = 0;
	fuellstand = 0;
}


Wassertank::~Wassertank()
{
}

void Wassertank::setGroesse(int menge)
{
	groesse = menge;
}

int Wassertank::getFuellstand()
{
	return fuellstand;
}

void Wassertank::ausleeren()
{
	fuellstand = 0;
}

void Wassertank::fuellen(int menge)
{
	if (fuellstand + menge < groesse) {
		fuellstand += menge;
	}
	else {
		fuellstand = groesse;
	}
}
