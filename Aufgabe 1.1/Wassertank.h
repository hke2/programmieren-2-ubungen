#pragma once
class Wassertank
{
public:
	Wassertank();
	~Wassertank();

	void setGroesse(int menge);

	int getFuellstand();

	void ausleeren();
	void fuellen(int menge);

private:
	int groesse;
	int fuellstand;
};

