// Aufgabe 1.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Spiel.h"

int main()
{
	Spiel* dasSpiel = new Spiel(20);
	
	do {
		dasSpiel->macheZug();
		getchar();
	} while (!dasSpiel->getStand());

	delete dasSpiel;

    return 0;
}

