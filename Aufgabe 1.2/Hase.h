#pragma once
class Hase
{
public:
	Hase();
	~Hase();

	bool ziehe(int felder);

	int getPosition();
	int getKarotten();
	int getVorrat();

private:
	int karotten;
	int position;
	int fehlversuche;
};

