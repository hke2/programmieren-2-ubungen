#include "stdafx.h"
#include "Igel.h"


Igel::Igel()
{
	salat = 10;
	position = 0;
}


Igel::~Igel()
{
}

bool Igel::ziehe(int felder)
{
	if (salat < felder) {
		salat = 10;
		return false;
	}
	else {
		position += felder;
		salat -= felder;
		return true;
	}
}

int Igel::getPosition()
{
	return position;
}

int Igel::getSalat()
{
	return salat;
}
