#pragma once
class Igel
{
public:
	Igel();
	~Igel();

	bool ziehe(int felder);

	int getPosition();
	int getSalat();
	int getVorrat();

private:
	int salat;
	int position;
};

