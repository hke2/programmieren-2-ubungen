#include "stdafx.h"
#include "Spiel.h"
#include <stdlib.h>
#include <time.h>


Spiel::Spiel(int anzahlFelder)
{
	ziel = anzahlFelder;
	derHase = new Hase();
	derIgel = new Igel();
}

Spiel::~Spiel()
{
	delete derHase;
	delete derIgel;
}

int Spiel::getZiel()
{
	return ziel;
}

int Spiel::wuerfle()
{
	srand((unsigned)time(NULL));
	return rand() % 6 + 1;
}

void Spiel::macheZug()
{
	derHase->ziehe(wuerfle());
	getchar();
	derIgel->ziehe(wuerfle());
}

bool Spiel::getStand()
{
	printf("Der Hase steht auf feld %d, der Igel steht auf feld %d", derHase->getPosition(), derIgel->getPosition());
	return (derHase->getPosition() >= ziel || derIgel->getPosition() >= ziel);
}
