#pragma once

#include "Hase.h"
#include "Igel.h"

class Spiel
{
public:
	Spiel(int anzahlFelder);
	~Spiel();

	int getZiel();
	int wuerfle();
	void macheZug();
	bool getStand();

private:
	Hase* derHase;
	Igel* derIgel;

	int ziel;
};

