// Aufgabe 2.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Pfad.h"


int main()
{
	Pfad* p = new Pfad(5);
	Punkt* p1 = new Punkt(1, 1);
	Punkt* p2 = new Punkt(2, 2);
	Punkt* p3 = new Punkt(3, 3);


	p->insertKoord(*p1);
	p->insertKoord(*p2);
	p->insertKoord(*p3);

	p->printKoord(0);
	p->printKoord(1);
	p->printKoord(2);

	getchar();

	delete p;

    return 0;
}

