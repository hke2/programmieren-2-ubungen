#include "stdafx.h"
#include "Pfad.h"


Pfad::Pfad(int MaxLaenge)
{
	aPunkte = new Punkt[MaxLaenge];
	AnzPunkte = 0;
}


Pfad::~Pfad()
{
	delete[] aPunkte;
}

void Pfad::insertKoord(Punkt & p)
{
	aPunkte[AnzPunkte] = p;
	AnzPunkte++;
}

void Pfad::printKoord(int i)
{
	aPunkte[i].print();
}
