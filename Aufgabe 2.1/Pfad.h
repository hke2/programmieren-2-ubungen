#pragma once
#include "Punkt.h"

class Pfad
{
public:
	Pfad(int MaxLaenge);
	~Pfad();

	void insertKoord(Punkt& p);
	void printKoord(int i);

private:
	Punkt* aPunkte;

	int AnzPunkte;
};

