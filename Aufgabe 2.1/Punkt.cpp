#include "stdafx.h"
#include "Punkt.h"
#include <iostream>


Punkt::Punkt(int x, int y)
{
	_x = x;
	_y = y;
}


Punkt::~Punkt()
{
}

void Punkt::print()
{
	printf("(%d, %d)\n", _x, _y);
}
