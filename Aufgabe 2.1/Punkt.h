#pragma once
class Punkt
{
public:
	Punkt(int x = 0, int y = 0);
	~Punkt();

	void print();

private:
	int _x;
	int _y;
};

