#include "stdafx.h"
#include "Hase.h"


Hase::Hase()
{
	position = 0;
	karotten = 31;
	fehlversuche = 0;
}


Hase::~Hase()
{
}

bool Hase::ziehe(int felder)
{
	int benötigteKarotten = 0;
	for (int i = 0; i <= felder; i++) {
		benötigteKarotten += i;
	}

	if (karotten >= benötigteKarotten) {
		position += felder;
		karotten -= benötigteKarotten;
		return true;
	}
	else {
		if (fehlversuche >= 3) {
			position -= felder;
			karotten += felder * 10;
			fehlversuche = 0;
		}
		else {
			fehlversuche += 1;
		}

		return false;
	}
}

int Hase::getKarotten()
{
	return karotten;
}

int Hase::getVorrat() {
	return getKarotten();
}
