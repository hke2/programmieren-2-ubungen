#pragma once
#include "Spielfiegur.h"

class Hase: Spielfiegur
{
public:
	Hase();
	~Hase();

	bool ziehe(int felder) override;

	int getKarotten();
	int getVorrat() override;

private:
	int karotten;
	int fehlversuche;
};

