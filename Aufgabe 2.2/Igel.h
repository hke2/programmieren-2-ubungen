#pragma once
#include "Spielfiegur.h"

class Igel: Spielfiegur
{
public:
	Igel();
	~Igel();

	bool ziehe(int felder) override;

	int getSalat();
	int getVorrat() override;

private:
	int salat;
};

