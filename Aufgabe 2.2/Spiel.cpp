#include "stdafx.h"
#include "Spiel.h"
#include <stdlib.h>
#include <time.h>
#include "Hase.h"
#include "Igel.h"


Spiel::Spiel(int anzahlFelder, int anzahlHasen, int anzahlIgel)
{
	ziel = anzahlFelder;
	figuren = new Spielfiegur*[anzahlHasen + anzahlIgel];
	this->anzahlHasen = anzahlHasen;
	this->anzahlIgel = anzahlIgel;

	for (int i = 0; i <= anzahlHasen; i++) {
		figuren[i] = (Spielfiegur*)new Hase();
	}

	for (int i = anzahlHasen; i <= anzahlHasen + anzahlIgel; i++) {
		figuren[i] = (Spielfiegur*)new Igel();
	}
}

Spiel::~Spiel()
{
	delete[] figuren;
}

int Spiel::getZiel()
{
	return ziel;
}

int Spiel::wuerfle()
{
	srand((unsigned)time(NULL));
	return rand() % 6 + 1;
}

void Spiel::macheZug()
{
	for (int i = 0; i < anzahlHasen + anzahlIgel; i++) {
		figuren[i]->ziehe(wuerfle());
		getchar();
	}
}

bool Spiel::getStand()
{
	for (int i = 0; i < anzahlHasen + anzahlIgel; i++) {
		printf("Spieler %d steht auf feld %d\n", i, figuren[i]->getPosition());
		if (figuren[i]->getPosition() >= ziel) {
			return true;
		}
	}

	return false;
}
