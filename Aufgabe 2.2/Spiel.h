#pragma once

#include "Spielfiegur.h"

class Spiel
{
public:
	Spiel(int anzahlFelder, int anzahlHasen, int anzahlIgel);
	~Spiel();

	int getZiel();
	int wuerfle();
	void macheZug();
	bool getStand();

private:
	int ziel;
	int anzahlHasen;
	int anzahlIgel;

	Spielfiegur** figuren;
};

