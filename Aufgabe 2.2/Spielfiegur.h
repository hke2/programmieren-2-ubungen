#pragma once
class Spielfiegur
{
public:
	Spielfiegur() {};
	~Spielfiegur() {};

	virtual int getVorrat() {
		return false;
	};

	virtual bool ziehe(int felder) =0;

	int getPosition() {
		return position;
	};

protected:
	int position;
};

