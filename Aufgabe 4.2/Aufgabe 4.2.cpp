// Aufgabe 1.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Spiel.h"

int main()
{
	Spiel* dasSpiel = new Spiel(30, 1, 1);
	
	do {
		dasSpiel->macheZug();
	} while (!dasSpiel->getStand());

	getchar();

	//delete dasSpiel;

    return 0;
}