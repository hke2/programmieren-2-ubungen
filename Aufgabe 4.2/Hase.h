#pragma once
#include "Spielfigur.h"

class Hase: Spielfigur
{
public:
	Hase();
	~Hase();

	bool ziehe(int felder) override;

	int getKarotten();
	int getVorrat() override;

	void bekommeVorratZurück();

private:
	int karotten;
	int fehlversuche;
};

