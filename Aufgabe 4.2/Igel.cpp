#include "stdafx.h"
#include "Igel.h"


Igel::Igel()
{
	salat = 10;
	position = 0;
}


Igel::~Igel()
{
}

bool Igel::ziehe(int felder)
{
	if (salat < felder) {
		salat = 10;
		letzterVerbrauchterVorrat = 0;
		return false;
	}
	else {
		position += felder;
		salat -= felder;
		letzterVerbrauchterVorrat = felder;
		return true;
	}
}

int Igel::getSalat()
{
	return salat;
}

int Igel::getVorrat() {
	return getSalat();
}

void Igel::bekommeVorratZurück()
{
	salat += letzterVerbrauchterVorrat;
}
