#pragma once
#include "Spielfigur.h"

class Igel: Spielfigur
{
public:
	Igel();
	~Igel();

	bool ziehe(int felder) override;

	int getSalat();
	int getVorrat() override;

	void bekommeVorratZurück() override;

private:
	int salat;
};

