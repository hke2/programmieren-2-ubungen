#pragma once
#include "Spielfeld.h"

class Kostenlos_Feld :
	public Spielfeld
{
public:
	Kostenlos_Feld();
	~Kostenlos_Feld();

	void trigger(Spielfigur* figur) override;
};

