#include "stdafx.h"
#include "Spiel.h"
#include <stdlib.h>
#include <time.h>
#include "Hase.h"
#include "Igel.h"

#include "Ziel.h"
#include "Kostenlos_Feld.h"
#include "Zur�ck_Feld.h"
#include "ZweiterZug_Feld.h"

Spiel::Spiel(int anzahlFelder, int anzahlHasen, int anzahlIgel)
{
	srand(time(NULL));

	ziel = anzahlFelder;
	figuren = new Spielfigur*[anzahlHasen + anzahlIgel];
	felder = new Spielfeld*[anzahlFelder];

	this->anzahlHasen = anzahlHasen;
	this->anzahlIgel = anzahlIgel;

	for (int i = 0; i <= anzahlHasen; i++) {
		figuren[i] = (Spielfigur*)new Hase();
	}

	for (int i = anzahlHasen; i <= anzahlHasen + anzahlIgel; i++) {
		figuren[i] = (Spielfigur*)new Igel();
	}

	for (int i = 0 ; i < anzahlFelder; i++) {
		int rnd = rand() % 4;
		switch (rand() % 4 +1){
		case 1:
			felder[i] = new Spielfeld();
			printf("Spielfeld %d: normal\n", i);
			break;
		case 2:
			felder[i] = new Kostenlos_Feld();
			printf("Spielfeld %d: kostenlos\n", i);
			break;
		case 3:
			felder[i] = new Zur�ck_Feld();
			printf("Spielfeld %d: zuruek\n", i);
			break;
		case 4:
			felder[i] = new ZweiterZug_Feld();
			printf("Spielfeld %d: zweiter zug\n", i);
			break;
		}
	}

	felder[anzahlFelder] = new Ziel();
	printf("Spielfeld %d: ziel\n", anzahlFelder);
}

Spiel::~Spiel()
{
	delete[] figuren;
}

int Spiel::getZiel()
{
	return ziel;
}

int Spiel::wuerfle()
{
	return rand() % 6 + 1;
}

void Spiel::macheZug()
{
	for (int i = 0; i < anzahlHasen + anzahlIgel; i++) {
		if (figuren[i]->ziehe(wuerfle()) && figuren[i]->getPosition() <= ziel && figuren[i]->getPosition() >= 0) {
			felder[figuren[i]->getPosition()]->trigger(figuren[i]);
		}
	}
}

bool Spiel::getStand()
{
	for (int i = 0; i < anzahlHasen + anzahlIgel; i++) {
		printf("Spieler %d steht auf feld %d und hat %d vorrat\n", i, figuren[i]->getPosition(), figuren[i]->getVorrat());
		if (figuren[i]->getPosition() >= ziel) {
			return true;
		}
	}

	return false;
}