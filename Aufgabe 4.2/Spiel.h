#pragma once

#include "Spielfigur.h"
#include "Spielfeld.h"

class Spiel
{
public:
	Spiel(int anzahlFelder, int anzahlHasen, int anzahlIgel);
	~Spiel();

	int getZiel();
	static int wuerfle();
	void macheZug();
	bool getStand();

private:
	int ziel;
	int anzahlHasen;
	int anzahlIgel;

	Spielfigur** figuren;
	Spielfeld** felder;
};

