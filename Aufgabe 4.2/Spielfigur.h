#pragma once
class Spielfigur
{
public:
	Spielfigur() {};
	~Spielfigur() {};

	virtual int getVorrat() = 0;

	virtual bool ziehe(int felder) = 0;

	int getPosition() {
		return position;
	};

	void setPosition(int position) {
		this->position = position;
	}

	virtual void bekommeVorratZurück() = 0;

protected:
	int position;
	int letzterVerbrauchterVorrat;
};

