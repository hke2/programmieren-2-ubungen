#pragma once
#include "Spielfeld.h"
class Ziel :
	public Spielfeld
{
public:
	Ziel();
	~Ziel();

	void trigger(Spielfigur* figur) override;
};

