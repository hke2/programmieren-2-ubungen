#pragma once
#include "Spielfeld.h"
class ZweiterZug_Feld :
	public Spielfeld
{
public:
	ZweiterZug_Feld();
	~ZweiterZug_Feld();

	void trigger(Spielfigur* figur) override;
};

