// Aufgabe 5.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Wertpapier.h"
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <thread>

int main()
{
	srand(time(NULL));

	Wertpapier* papiere[10];

	for (int i = 0; i < (sizeof(papiere) / sizeof(papiere[0])); i++) {
		papiere[i] = new Wertpapier("Wertpapier", i);
	}

	int alarm_lo = 20;
	int alarm_up = 80;

	while (true) {
		for (int i = 0; i < (sizeof(papiere) / sizeof(papiere[0])); i++) {
			papiere[i]->setKurs(rand() % 100 );
			printf("%s %d: %02d", papiere[i]->getName(), papiere[i]->getIsin(), papiere[i]->getKurs());

			if (papiere[i]->getKurs() >= alarm_up || papiere[i]->getKurs() <= alarm_lo) {
				printf(" <--\n");
			}
			else {
				printf("\n");
			}
		}
		printf("\n\n\n\n\n\n\n\n");
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}


    return 0;
}

