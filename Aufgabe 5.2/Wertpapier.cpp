#include "stdafx.h"
#include "Wertpapier.h"


Wertpapier::Wertpapier(char name[], int isin)
{
	this->name = name;
	this->isin = isin;
	this->kurs = 0;
}

Wertpapier::~Wertpapier()
{
}

char * Wertpapier::getName()
{
	return this->name;
}

int Wertpapier::getIsin()
{
	return this->isin;
}

int Wertpapier::getKurs()
{
	return this->kurs;
}

void Wertpapier::setKurs(int kurs)
{
	this->kurs = kurs;
}
