#pragma once
class Wertpapier
{
public:
	Wertpapier(char name[], int isin);
	~Wertpapier();

	char* getName();
	int getIsin();
	int getKurs();

	void setKurs(int kurs);
private:
	char* name;
	int isin;
	int kurs;

};

