#include "stdafx.h"
#include "Addition.h"


Addition::Addition(Komponente* left, Komponente* right)
{
	this->left = left;
	this->right = right;
}


Addition::~Addition()
{
}

int Addition::Wert()
{
	return left->Wert() + right->Wert();
}
