#pragma once
#include "Kompositum.h"

class Addition: public Kompositum
{
public:
	Addition(Komponente* left, Komponente* right);
	~Addition();

	int Wert() override;
};

