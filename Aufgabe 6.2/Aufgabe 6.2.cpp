// Aufgabe 6.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Addition.h"
#include "Multiplikation.h"


int main()
{
	Komponente* val1 = new Komponente(1);
	Komponente* val2 = new Komponente(2);

	Addition* add = new Addition(val1, val2);
	Multiplikation* mul = new Multiplikation(add, val2);
	

	add->Print();
	mul->Print();
	getchar();

    return 0;
}

