#include "stdafx.h"
#include "Komponente.h"


Komponente::Komponente(int value)
{
	this->value = value;
}


Komponente::~Komponente()
{
}

int Komponente::Wert()
{
	return value;
}

void Komponente::Print()
{
	printf("%d\n", Wert());
}
