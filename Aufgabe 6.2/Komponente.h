#pragma once
class Komponente
{
public:
	Komponente() {};

	Komponente(int value);
	~Komponente();

	virtual int Wert();

	void Print();

private:
	int value;
};

