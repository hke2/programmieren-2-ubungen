#pragma once
#include "Komponente.h"

class Kompositum: public Komponente
{
public:
	Kompositum() {};
	Kompositum(Komponente* left, Komponente* right);
	~Kompositum();

	Komponente* left;
	Komponente* right;
};

