#include "stdafx.h"
#include "Multiplikation.h"


Multiplikation::Multiplikation(Komponente * left, Komponente * right)
{
	this->left = left;
	this->right = right;
}

Multiplikation::~Multiplikation()
{
}

int Multiplikation::Wert()
{
	return left->Wert() * right->Wert();
}
