#pragma once
#include "Kompositum.h"

class Multiplikation: public Kompositum
{
public:
	Multiplikation(Komponente* left, Komponente* right);
	~Multiplikation();

	int Wert() override;
};

